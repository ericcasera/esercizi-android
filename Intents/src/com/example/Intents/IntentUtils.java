package com.example.Intents;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

/**
 * User: Eric
 * Date: 08/04/13
 * Time: 12.19
 */
public class IntentUtils {

    public static void invokeWebBrowser(Activity activity) {
        Intent intent=new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://latemar.science.unitn.it"));
        activity.startActivity(intent);
    }

    public static void invokeWebSearch(Activity activity) {
        Intent intent=new Intent(Intent.ACTION_WEB_SEARCH,Uri.parse("http://www.google.com"));
        activity.startActivity(intent);
        }

    public static void dial(Activity activity) {
        Intent intent=new Intent(Intent.ACTION_DIAL);
        activity.startActivity(intent);
    }

    public static void showDirections(Activity activity){
        Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("http://maps.google.com/maps? saddr=Bolzano&daddr=Trento"));
        activity.startActivity(intent);
    }


}
