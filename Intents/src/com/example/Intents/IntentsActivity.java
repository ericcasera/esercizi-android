package com.example.Intents;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class IntentsActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);
        int base= Menu.FIRST;
        MenuItem item1=menu.add(base,1,1,"invokeWebBrowser-VIEW");
        MenuItem item2=menu.add(base,2,2,"invokeWebBrowser-SEARCH");
        MenuItem item3=menu.add(base,3,3,"showDirections");
        MenuItem item5=menu.add(base,4,4,"dial");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        System.err.println("item="+item.getItemId());
        if (item.getItemId()==1)
            IntentUtils.invokeWebBrowser(this);
        else if (item.getItemId()==2)
            IntentUtils.invokeWebSearch(this);
        else if (item.getItemId()==3)
            IntentUtils.showDirections(this);
        else if (item.getItemId()==4)
            IntentUtils.dial(this);
        else
            return super.onOptionsItemSelected(item);
        return true;
    }
}
